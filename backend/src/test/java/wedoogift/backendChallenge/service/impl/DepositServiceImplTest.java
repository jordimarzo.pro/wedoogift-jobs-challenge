package wedoogift.backendChallenge.service.impl;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.boot.test.context.SpringBootTest;
import wedoogift.backendChallenge.model.Company;
import wedoogift.backendChallenge.model.User;

import static org.junit.jupiter.api.Assertions.*;
import static wedoogift.backendChallenge.constant.DepositConstant.TYPE_GIFT_DEPOSIT;
import static wedoogift.backendChallenge.constant.DepositConstant.TYPE_MEAL_DEPOSIT;

@SpringBootTest
class DepositServiceImplTest {

    @InjectMocks
    private DepositServiceImpl subject;

    @Test
    void depositAmountGiftTest() throws Exception {
        User result = new User();
        Company company = new Company();
        company.setBalance(200.0);

        subject.depositAmount(result, company, 150.0, TYPE_GIFT_DEPOSIT);

        assertEquals(1, result.getDepositList().size());
        assertEquals(50.0, company.getBalance());
    }

    @Test
    void depositAmountMealTest() throws Exception {
        User user = new User();
        Company company = new Company();
        company.setBalance(200.0);

        User result = subject.depositAmount(user, company, 150.0, TYPE_MEAL_DEPOSIT);

        assertEquals(1, result.getDepositList().size());
        assertEquals(50.0, company.getBalance());
    }

    @Test
    void depositAmountLessZeroTest() {
        User user = new User();
        Company company = new Company();
        try {
            subject.depositAmount(user, company, -100.0, TYPE_MEAL_DEPOSIT);
            fail("le test aurait dû retouner une exception");
        } catch (Exception e) {
            assertEquals("Le montant du dépôt doit être supérieur à zéro", e.getMessage());
        }
    }

    @Test
    void depositAmountInsufficientBalanceTest() {
        User user = new User();
        Company company = new Company();
        company.setBalance(100.0);
        try {
            subject.depositAmount(user, company, 250.0, TYPE_GIFT_DEPOSIT);
            fail("le test aurait dû retouner une exception");
        } catch (Exception e) {
            assertEquals("Une erreur est survenue lors du retrait du montant sur le solde de l'entreprise", e.getMessage());
            assertEquals(100.0, company.getBalance());
            assertTrue(user.getDepositList().isEmpty());
        }
    }

    @Test
    void getBalanceForUserTest() throws Exception {
        User result = new User();
        Company company = new Company();
        company.setBalance(1000.0);

        subject.depositAmount(result, company, 150.0, TYPE_GIFT_DEPOSIT);
        subject.depositAmount(result, company, 50.0, TYPE_GIFT_DEPOSIT);
        subject.depositAmount(result, company, 80.0, TYPE_MEAL_DEPOSIT);
        subject.depositAmount(result, company, 20.0, TYPE_MEAL_DEPOSIT);

        assertEquals(4, result.getDepositList().size());
        assertEquals(300.0, subject.getBalanceForUser(result));
    }

    @Test
    void getBalanceForUserAndTypeDepositeTest() throws Exception {
        User result = new User();
        Company company = new Company();
        company.setBalance(1000.0);

        subject.depositAmount(result, company, 150.0, TYPE_GIFT_DEPOSIT);
        subject.depositAmount(result, company, 50.0, TYPE_GIFT_DEPOSIT);
        subject.depositAmount(result, company, 80.0, TYPE_MEAL_DEPOSIT);
        subject.depositAmount(result, company, 20.0, TYPE_MEAL_DEPOSIT);

        assertEquals(4, result.getDepositList().size());
        assertEquals(100.0, subject.getBalanceForUserAndTypeDeposite(result, TYPE_MEAL_DEPOSIT));
        assertEquals(200.0, subject.getBalanceForUserAndTypeDeposite(result, TYPE_GIFT_DEPOSIT));
    }

    @Test
    void getBalanceForUserAndTypeDepositeOtherTest() throws Exception {
        User result = new User();
        Company company = new Company();
        company.setBalance(1000.0);

        subject.depositAmount(result, company, 150.0, TYPE_GIFT_DEPOSIT);
        subject.depositAmount(result, company, 50.0, TYPE_GIFT_DEPOSIT);
        subject.depositAmount(result, company, 80.0, TYPE_MEAL_DEPOSIT);
        subject.depositAmount(result, company, 20.0, TYPE_MEAL_DEPOSIT);

        assertEquals(4, result.getDepositList().size());
        assertEquals(0.0, subject.getBalanceForUserAndTypeDeposite(result, "otherTest"));
    }
}