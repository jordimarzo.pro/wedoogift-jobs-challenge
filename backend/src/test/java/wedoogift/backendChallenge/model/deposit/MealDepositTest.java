package wedoogift.backendChallenge.model.deposit;

import org.junit.jupiter.api.Test;
import wedoogift.backendChallenge.model.Company;

import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;

import static org.junit.jupiter.api.Assertions.*;

class MealDepositTest {

    @Test
    void mealDepositTest() {
        Company company = new Company();
        Deposit result = new MealDeposit(50.0, company);

        LocalDate now = LocalDate.now();
        LocalDate expirationDate = now.plusYears(1).withMonth(2).with(TemporalAdjusters.lastDayOfMonth());

        assertEquals(50.0, result.getAmount());
        assertEquals(company, result.getFromCompany());
        assertEquals(now, result.getDepositDate(), "getDepositDate aurait dû retourner la date du jour.");
        assertEquals(expirationDate, result.getExpirationDate(),
                "getExpirationDate aurait dû retourner la date du derniers jours du mois de février de l'année suivante");
    }
}