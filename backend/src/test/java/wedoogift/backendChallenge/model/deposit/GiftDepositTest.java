package wedoogift.backendChallenge.model.deposit;

import org.junit.jupiter.api.Test;
import wedoogift.backendChallenge.model.Company;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class GiftDepositTest {

    @Test
    void giftDepositTest() {
        Company company = new Company();
        Deposit result = new GiftDeposit(50.0, company);

        LocalDate now = LocalDate.now();
        LocalDate expirationDate = now.plusDays(365);

        assertEquals(50.0, result.getAmount());
        assertEquals(company, result.getFromCompany());
        assertEquals(now, result.getDepositDate(), "getDepositDate aurait dû retourner la date du jour.");
        assertEquals(expirationDate, result.getExpirationDate(),
                "getExpirationDate aurait dû retourner la date du jour + 365 jours");
    }

}