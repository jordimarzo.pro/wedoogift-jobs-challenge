package wedoogift.backendChallenge.service;

import wedoogift.backendChallenge.model.Company;
import wedoogift.backendChallenge.model.User;

import java.math.BigDecimal;

public interface DepositService {

    User depositAmount(User user, Company company, Double amount, String typeOfDeposit) throws Exception;

    Double getBalanceForUser(User user);

    Double getBalanceForUserAndTypeDeposite(User user, String typeOfDeposit);
}
