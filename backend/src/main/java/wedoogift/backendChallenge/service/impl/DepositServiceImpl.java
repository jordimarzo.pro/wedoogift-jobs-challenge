package wedoogift.backendChallenge.service.impl;

import org.springframework.stereotype.Service;
import wedoogift.backendChallenge.constant.DepositConstant;
import wedoogift.backendChallenge.model.Company;
import wedoogift.backendChallenge.model.User;
import wedoogift.backendChallenge.model.deposit.Deposit;
import wedoogift.backendChallenge.model.deposit.GiftDeposit;
import wedoogift.backendChallenge.model.deposit.MealDeposit;
import wedoogift.backendChallenge.service.DepositService;

import java.time.LocalDate;

@Service
public class DepositServiceImpl implements DepositService {

    /**
     * Fonction servant à faire un dépôt sur le compte d'un bénéficiaire
     * @param user Bénéficiaire
     * @param company Entreprise émétrice
     * @param amount Montant
     * @param typeOfDeposit Type de dépôt (Gift ou Meal)
     * @return le bénéficiaire mis à jour
     * @throws Exception retourne une exception si le montant est inférieur ou égal à zéro
     */
    @Override
    public User depositAmount(User user, Company company, Double amount, String typeOfDeposit) throws Exception {

        if (amount <= 0) {
            throw new Exception("Le montant du dépôt doit être supérieur à zéro");
        }

        Deposit deposit = null;

        try {
            removeAmountForCompany(amount, company);

            switch (typeOfDeposit) {
                case DepositConstant.TYPE_MEAL_DEPOSIT:
                    deposit = new MealDeposit(amount, company);
                    break;

                case DepositConstant.TYPE_GIFT_DEPOSIT:
                    deposit = new GiftDeposit(amount, company);
                    break;
            }
        } catch (Exception e) {
            throw new Exception("Une erreur est survenue lors du retrait du montant sur le solde de l'entreprise", e);
        }

        user.addDeposit(deposit);

        return user;
    }

    /**
     * Indique le solde total de l'utilisateur
     * @param user Utilisateur
     * @return le solde total de l'utilisateur
     */
    @Override
    public Double getBalanceForUser(User user) {
        return user.getDepositList().stream()
                .filter(deposit -> deposit.getExpirationDate().isAfter(LocalDate.now()))
                .mapToDouble(Deposit::getAmount)
                .sum();
    }

    /**
     * Indique le solde de l'utilisateur selon le type de dépôt (Gift ou Meal)
     * @param user Utilisateur
     * @param typeOfDeposit type de dépôt
     * @return le solde de l'utilisateur selon le type de dépôt (Gift ou Meal)
     */
    @Override
    public Double getBalanceForUserAndTypeDeposite(User user, String typeOfDeposit) {
        switch (typeOfDeposit) {
            case DepositConstant.TYPE_MEAL_DEPOSIT:
                return user.getDepositList().stream()
                        .filter(deposit -> deposit.getExpirationDate().isAfter(LocalDate.now()) && MealDeposit.class.equals(deposit.getClass()))
                        .mapToDouble(Deposit::getAmount)
                        .sum();

            case DepositConstant.TYPE_GIFT_DEPOSIT:
                return user.getDepositList().stream()
                        .filter(deposit -> deposit.getExpirationDate().isAfter(LocalDate.now()) && GiftDeposit.class.equals(deposit.getClass()))
                        .mapToDouble(Deposit::getAmount)
                        .sum();

            default:
                return 0.0;
        }
    }

    /**
     * fonction servant à soustraire le montant d'un dépôt au solde de l'entreprise
     * @param amount Montant à soustraire
     * @param company Entreprise
     * @throws Exception retourne une exception si le solde de l'entreprise n'est pas suffisant
     */
    private void removeAmountForCompany(Double amount, Company company) throws Exception {
        if (company.getBalance() - amount >= 0) {
            company.setBalance(company.getBalance() - amount);
        } else {
            throw new Exception("Le solde de l'entreprise n'est pas suffisant !");
        }
    }
}
