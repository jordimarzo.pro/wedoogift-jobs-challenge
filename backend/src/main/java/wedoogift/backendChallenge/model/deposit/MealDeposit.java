package wedoogift.backendChallenge.model.deposit;

import wedoogift.backendChallenge.model.Company;

import java.time.temporal.TemporalAdjusters;

public class MealDeposit extends Deposit {
    public MealDeposit(Double amount, Company fromCompany) {
        super(amount, fromCompany);
    }

    /**
     * Définit la date d'expiration du dépôt le dernier jour du mois de février de l'année suivante.
     */
    @Override
    public void defineExpirationDate() {
        this.setExpirationDate(this.getDepositDate().plusYears(1).withMonth(2).with(TemporalAdjusters.lastDayOfMonth()));
    }
}
