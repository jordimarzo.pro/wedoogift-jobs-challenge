package wedoogift.backendChallenge.model.deposit;

import wedoogift.backendChallenge.model.Company;

public class GiftDeposit extends Deposit {
    public GiftDeposit(Double amount, Company fromCompany) {
        super(amount, fromCompany);
    }

    /**
     * Définit la date d'expiration du dépôt 365 jours après la date de dépôt.
     */
    @Override
    public void defineExpirationDate() {
        this.setExpirationDate(this.getDepositDate().plusDays(365));
    }
}
