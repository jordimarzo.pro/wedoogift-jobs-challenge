package wedoogift.backendChallenge.model.deposit;

import wedoogift.backendChallenge.model.Company;

import java.time.LocalDate;

public abstract class Deposit {

    private Double amount;
    private final LocalDate depositDate = LocalDate.now();
    private LocalDate expirationDate;
    private Company fromCompany;

    public Deposit(Double amount, Company fromCompany) {
        this.amount = amount;
        this.fromCompany = fromCompany;
        defineExpirationDate();
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public LocalDate getDepositDate() {
        return depositDate;
    }

    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    protected void setExpirationDate(LocalDate expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Company getFromCompany() {
        return fromCompany;
    }

    public void setFromCompany(Company fromCompany) {
        this.fromCompany = fromCompany;
    }

    public abstract void defineExpirationDate();
}
