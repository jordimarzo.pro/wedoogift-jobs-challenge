package wedoogift.backendChallenge.model;

import wedoogift.backendChallenge.model.deposit.Deposit;

import java.util.ArrayList;
import java.util.List;

public class User {

    private Integer id;
    private String lastname;
    private String firstname;
    private List<Deposit> depositList = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public List<Deposit> getDepositList() {
        return depositList;
    }

    public void setDepositList(List<Deposit> depositList) {
        this.depositList = depositList;
    }

    public void addDeposit(Deposit deposit){
        this.depositList.add(deposit);
    }
}
