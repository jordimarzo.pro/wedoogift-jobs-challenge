package wedoogift.backendChallenge.constant;

public class DepositConstant {
    public static final String TYPE_MEAL_DEPOSIT = "Meal";
    public static final String TYPE_GIFT_DEPOSIT = "Gift";
}
